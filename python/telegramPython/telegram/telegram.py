import telebot
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

class Telegram():
    def __init__ (self, TOKEN):
        self.TOKEN = TOKEN
        bot = telebot.TeleBot(self.TOKEN)

        chatbot = ChatBot('Example Bot')
        trainer = ChatterBotCorpusTrainer(chatbot)

        @bot.message_handler(commands=['start', 'help'])
        def send_welcome(message):
            bot.reply_to(message, "Hello to chat bot")

        @bot.message_handler(func=lambda m: True)
        def echo_all(message):
            answer = chatbot.get_response(message.text)
            bot.reply_to(message, answer)

        bot.polling()
