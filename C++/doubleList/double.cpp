#include "double.h"
#include <iostream>
using namespace std;

list::list(){
	first = NULL;
	last = NULL;
}

list::~list(){
	delete first;
	delete last;
}

void list::insertNode(int data){
	node *newNode = new node();
	newNode -> data = data;
	if (first == NULL) {
		first = newNode;
		first -> next = NULL;
		first -> back = NULL;
		last = newNode;
	} else {
		last -> next = newNode;
		newNode -> next = NULL;
		newNode -> back = last;
		last = newNode;
	}
}

bool list::searchNode(int search){
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		while (aux != NULL) {
			if (aux -> data == search)
				return true;
			aux = aux -> next;
		}
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool list::changeNode(int search) {
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		while (aux != NULL){
			if (aux -> data == search) {
				cout << "Insert new date to change: ";
				cin >> aux -> data;
				return true;
			}
			aux = aux -> next;
		}
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool list::deleteNode(int search) {
	node *aux = new node();
	node *before = new node();
	before = NULL;
	aux = first;
	if (first != NULL){
		while (aux != NULL) {
			if (aux -> data == search) {
				if (aux == first) {
					first = first -> next;
					first -> back = NULL;
				} else if (aux == last){
					before -> next = NULL;
					last = before;
				} else 
					before -> next = aux -> next;
				return true;
			}
			before = aux;
			aux = aux -> next;
		}
	} else
		cout << endl << "The empty List" << endl;
	return false;
}


void list::printListOrigin(){
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		while (aux != NULL) {
			cout << aux -> data << " ";
			aux = aux -> next;
		}
		cout << endl;
	}
}

void list::printListReverse(){
	node *aux = new node();
	aux = last;
	if (first != NULL) {
		while (aux != NULL) {
			cout << aux -> data << " ";
			aux = aux -> back;
		}
		cout << endl;
	}
}
