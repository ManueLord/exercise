#ifndef __double__
#define __double__

typedef struct node {
	int data;
	node* next;
	node* back;
}node;


class list{
public:
	list();
	~list();

	node *first, *last;

	void insertNode(int);
	bool searchNode(int);
	bool changeNode(int);
	bool deleteNode(int);
	void printListOrigin();
	void printListReverse();
};

#endif //__double__
