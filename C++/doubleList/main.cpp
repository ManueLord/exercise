#include "double.cpp"
#include <iostream>
#include <string>
using namespace std;

int Menu(int option){
        cout << endl << "|--------------------------------------------|";
        cout << endl << "|                Double List                 |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "| 1.- Insert         | 5.- Print Origin      |";
        cout << endl << "| 2.- Search         | 6.- Print Reverse     |";
        cout << endl << "| 3.- Change         | 7.- Exit              |";
		cout << endl << "| 4.- Delete         |                       |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "Your optios is: ";
        cin >> option;
        return option;
}

int main(){

	list d;

	int o = 0, data = 0, search = 0;
    string valData = "";
    do{
        o = Menu(o);
        switch (o){
            case 1:
                cout << "Insert data to list: ";
                cin >> data;
                d.insertNode(data);
                cout << endl << "Correct ADD deta in list" << endl;
                break;
            case 2:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (d.searchNode(search) == 1)
                    valData = "True";
                else
                    valData = "False";
                cout << endl << "The data: " << search << " found is: " << valData << endl;
                break;
            case 3:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (d.changeNode(search) == 1)
                    valData = "Change data Node";
                else
                    valData = "Data is not found";
                cout << endl << valData << endl;
                break;
            case 4:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (d.deleteNode(search) == 1)
                    valData = "Data is delete";
                else
					valData = "Data no Delete";
                cout << endl << valData << endl;  
                break;
            case 5:
                cout << endl << "The List is:" << endl;
				d.printListOrigin();
				break;
            case 6:
				cout << endl << "The List is:" << endl;
				d.printListReverse();
                break;
			case 7:
                cout << endl << "Exit the program" << endl << endl;
                break;
            default:
                cout << endl << "No exists option" << endl;
                break;
        }
    }while( o != 7);

	return 0;
}

