#include "doubleCircular.h"
#include <iostream>
using namespace std;

circular::circular(){
	first = NULL;
	last = NULL;
}

circular::~circular(){
	delete first;
	delete last;
}

void circular::insertNode(int data) {
	node *newNode = new node();
	newNode -> data = data;
	if (first == NULL) {
		first = newNode;
		last = newNode;
		first -> next = first;
		first -> back = first;
	} else {
		last -> next = newNode;
		newNode -> back = last;
		newNode -> next = first;
		last = newNode;
		first -> back = last;
	}
}

bool circular::searchNode(int search) {
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search)
				return true;
			aux = aux -> next;
		} while (aux != first);
		cout << endl;
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool circular::changeNode(int search) {
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search) {
				cout << "Insert new date to change: ";
				cin >> aux -> data;
				return true;
			}
			aux = aux -> next;
		} while (aux != first);
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool circular::deleteNode(int search) {
	node *aux = new node();
	node *before = new node();
	before = NULL;
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search) {
				if (aux == first) {
					first = first -> next;
					last -> next = first;
					first -> back = last;
				} else if (aux == last) {
					before -> next = first;
					last = before;
				} else
					before -> next = aux -> next;
				return true;
			}
			before = aux;
			aux = aux -> next;
		} while (aux != first);
		cout << endl;
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

void circular::printListOriginal() {
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		do{
			cout << aux -> data << " ";
			aux = aux -> next;
		} while (aux != first);
		cout << endl;
	} else
		cout << endl << "The empty list" << endl;
}

void circular::printListReverse(){
	node *aux = new node();
	aux = last;
	if (first != NULL) {
		do {
			cout << aux -> data << " ";
			aux = aux -> back;
		} while (aux != last);
		cout << endl;
	} else
		cout << endl << "The empty list" << endl;
}
