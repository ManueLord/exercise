#ifndef __doubleCircular__
#define __doubleCircular__

typedef struct node{
	int data;
	node* next;
	node* back;
}node;

class circular{
public:
	circular();
	~circular();

	node *first, *last;

	void insertNode(int);
	bool searchNode(int);
	bool changeNode(int);
	bool deleteNode(int);
	void printListOriginal();
	void printListReverse();
};

#endif //__doubleCircular__
