#include "doubleCircular.cpp"
#include <iostream>
using namespace std;

int Menu(int option){
    cout << endl << "|--------------------------------------------|";
    cout << endl << "|           Double Circular List             |";
    cout << endl << "|--------------------------------------------|";
    cout << endl << "| 1.- Insert         | 5.- Print Origin      |";
    cout << endl << "| 2.- Search         | 6.- Print Reverse     |";
    cout << endl << "| 3.- Change         | 7.- Exit              |";
	cout << endl << "| 4.- Delete         |                       |";
    cout << endl << "|--------------------------------------------|";
    cout << endl << "Your optios is: ";
    cin >> option;
    return option;
}

int main(){
    
    circular dcl;

    int o = 0, data = 0, search = 0;

    do{
        o = Menu(o);
        switch (o){
            case 1:
                cout << "Insert data to list: ";
                cin >> data;
                dcl.insertNode(data);
                cout << endl << "Correct ADD deta in list" << endl;
                break;
            case 2:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (dcl.searchNode(search) == 1)
                    cout << endl << "The data: " << search << " found is: " << "True" << endl;
                else
                    cout << endl << "The data: " << search << " found is: " << "False" << endl;
                break;
            case 3:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (dcl.changeNode(search) == 1)
                    cout << endl << "Change data Node" << endl;
                else
                    cout << endl << "Data is not found" << endl;
                break;
            case 4:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (dcl.deleteNode(search) == 1)
                    cout << endl << "Data is delete" << endl;
                else
                    cout << endl << "Data no Delete" << endl;
                break;
            case 5:
                cout << endl << "The List is:" << endl;
				dcl.printListOriginal();
				break;
            case 6:
                cout << endl << "The List is:" << endl;
				dcl.printListReverse();
				break;
			case 7:
                cout << endl << "Exit the program" << endl << endl;
                break;
            default:
                cout << endl << "No exists option" << endl;
                break;
        }
    }while( o != 7);

    return 0;
}