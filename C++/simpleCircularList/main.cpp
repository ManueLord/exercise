#include "simpleCircular.cpp"
#include <iostream>
#include <string>
using namespace std;

int Menu(int option){
    cout << endl << "|--------------------------------------------|";
    cout << endl << "|            Simple List Circule             |";
    cout << endl << "|--------------------------------------------|";
    cout << endl << "| 1.- Insert         | 4.- Delete            |";
    cout << endl << "| 2.- Search         | 5.- Print             |";
    cout << endl << "| 3.- Change         | 6.- Exit              |";
    cout << endl << "|--------------------------------------------|";
    cout << endl << "Your optios is: ";
    cin >> option;
    return option;
}

int main(){
    simple scl;
    
    int o = 0, data = 0, search = 0;

    do{
        o = Menu(o);
        switch (o){
            case 1:
                cout << "Insert data to list: ";
                cin >> data;
                scl.insertNode(data);
                cout << endl << "Correct ADD deta in list" << endl;
                break;
            case 2:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (scl.searchNode(search) == 1)
                    cout << endl << "The data: " << search << " found is: " << "True" << endl;
                else
                    cout << endl << "The data: " << search << " found is: " << "False" << endl;
                break;
            case 3:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (scl.changeNode(search) == 1)
                    cout << endl << "Change data Node" << endl;
                else
                    cout << endl << "Data is not found" << endl;
                break;
            case 4:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (scl.deleteNode(search) == 1)
                    cout << endl << "Data is delete" << endl;
                else
                    cout << endl << "Data no Delete" << endl;
                break;
            case 5:
                cout << endl << "The List is:" << endl;
				scl.printList();
				break;
			case 6:
                cout << endl << "Exit the program" << endl << endl;
                break;
            default:
                cout << endl << "No exists option" << endl;
                break;
        }
    }while( o != 6);

	return 0;
}