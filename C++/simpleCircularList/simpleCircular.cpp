#include "simpleCircular.h"
#include <iostream>
using namespace std;

simple::simple(){
	first = NULL;
	last = NULL;
}

simple::~simple(){
	delete first;
	delete last;
}

void simple::insertNode(int data) {
	node *newNode = new node();
	newNode -> data = data;
	if (first == NULL) {
		first = newNode;
		first -> next = first;
		last = first;
	} else {
		last -> next = newNode;
		newNode -> next = first;
		last = newNode;
	}
}

bool simple::searchNode(int search){
	node *aux = new node();
	aux = first;
	if (first != NULL){
		do{
			if (aux -> data == search)
				return true;
			aux = aux -> next;
		} while (aux != first);
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool simple::changeNode(int search){
	node *aux = new node();
	aux = first;
	if (first != NULL){
		do{
			if (aux -> data == search){
				cout << "Insert new date to change: ";
				cin >> aux -> data;
				return true;
			}
			aux = aux -> next;
		} while (aux != first);
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

bool simple::deleteNode(int search){
	node *aux = new node();
	node *before = new node();
	before = NULL;
	aux = first;
	if (first != NULL){
		do{
			if (aux -> data == search){
				if (aux == first) {
					first = first -> next;
					last -> next = first;
					first -> back = last;
				} else if (aux == last) {
					before -> next = first;
					last = before;
				} else 
					before -> next = aux -> next;
				return true;
			}
			before = aux;
			aux = aux -> next;
		} while (aux != first);
	} else
		cout << endl << "The empty list" << endl;
	return false;
}

void simple::printList(){
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		do{
			cout << aux -> data << " ";
			aux = aux -> next;
		} while (aux != first);
		cout << endl;
	} else
		cout << endl << "The empty list" << endl;
}