#include <iostream>
#include <cstdlib>
using namespace std;

void arrayB1 (int ab[], int s){
	int count = 0, aux  = 0;
	do{
		count = 0;
		for (int i = 1; i < s; i++){
			if (ab[i-1] > ab[i]){
				aux = ab[i-1];
				ab[i-1] = ab[i];
				ab[i] = aux;
				count++;
			}
		}
	} while (count != 0);
	
	for (int i = 0; i < s; i++) {
		cout << ab[i] << "  ";
	}
}

void arrayB2 (int ab[], int s){
	int aux  = 0;
	for (int j = 0; j < s; j++){
		for (int i = 0; i < s -1; i++){
			if (ab[(i)] > ab[i+1]){
				aux = ab[i+1];
				ab[i+1] = ab[i];
				ab[i] = aux;
			}
		}
	}
	
	for (int i = 0; i < s; i++) {
		cout << ab[i] << "  ";
	}
}

int main(){
	int size = 0, insert = 0, type = 0;
	cout << "Insert size to array: ";
	cin >> size;
	int arrayBubble[size];
	do {
		cout << "Type:\n1 = Random\n2 = Manual\nSelect type insertion: ";
		cin >> insert;
		if (insert == 1 || insert == 2)
			type++;
	} while (type != 1);
	for (int i = 0; i < size; i++){
		if (insert == 1){
			arrayBubble[i] = (rand() % 100) + 1;
		} else {
			cout << "Insert data: ";
			cin >> arrayBubble[i];
		}
	}
	cout << endl << "The array is:" << endl;;
	for (int i = 0; i < size; i++) {
		cout << arrayBubble[i] << "  ";
	}
	cout << endl << endl << "The array order is:" << endl;
	arrayB1(arrayBubble, size);
	cout << endl << endl << "The array order is:" << endl;
	arrayB2(arrayBubble, size);
	cout << endl << endl;
	return 0;
}

