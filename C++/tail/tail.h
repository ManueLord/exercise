#ifndef __tail__
#define __tail__

typedef struct node{
	int data;
	node* next;
}node;

class tail{
public:
	tail();
	~tail();

	node *first, *last;

	void insertNode(int);
	bool searchNode(int);
	bool changeNode(int);
	bool deleteNode(int);
	void printTail();
};

#endif //__tail__
