#include "tail.h"
#include<iostream>
using namespace std;

tail::tail(){
	first = NULL;
}

tail::~tail(){
	delete first;
}

void tail::insertNode(int data){
	node *newNode = new node();
	newNode -> data = data;

	if (first == NULL){
		first = newNode;
		first -> next = NULL;
		last = first;
	} else {
		last -> next = newNode;
		newNode -> next = NULL;
		last = newNode;
	}
}

bool tail::searchNode(int search){
	node *present = new node();
	present = first;
	bool found = false;
	if (first != NULL){
		while (present != NULL && found != true){
			if (present -> data == search)
				found = true;
			present = present -> next;
		}
	} else 
		cout << endl  << "The tail empty" << endl;
	return found;
}

bool tail::changeNode(int search){
        node *present = new node();
        present = first;
        bool found = false;
        if (first != NULL){
            while (present != NULL && found != true){
                if (present -> data == search){
					cout << endl << "The data to change is: " << search << endl;
					cout << "The New data is: ";
                   	cin >> present -> data;
					found = true;
				}
                present = present -> next;
            }
        } else
            cout << endl  << "The tail empty" << endl;
        return found;
}

bool tail::deleteNode(int search){
	node *present = new node();
	node *before = new node();
	present = first;
	before = NULL;
	bool found = false;
	if (first != NULL){
		while (present != NULL && found != true){
			if (present -> data == search){
				if (present == first)
					first = first -> next;
				else if (present == last){
					before -> next = NULL;
					last = before;
				} else
					before -> next = present -> next;
				found = true;
			}
			before = present;
			present = present -> next;
		}
	} else 
		cout << endl  << "The tail empty" << endl;
	return found;
}

void tail::printTail(){
	node *present = new node();
	present = first;
	if(present != NULL){
		while (present != NULL){
			cout << present -> data << " ";
			present = present -> next;
		}
		cout << endl;
	} else {
		cout << endl << "the tail empty" << endl << endl;
	}
}
