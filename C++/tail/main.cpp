#include "tail.cpp"
#include <iostream>
#include <string>
using namespace std;

int Menu(int option){
        cout << endl << "|--------------------------------------------|";
        cout << endl << "|                   Tail                     |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "| 1.- Insert         | 4.- Delete            |";
        cout << endl << "| 2.- Search         | 5.- Print             |";
        cout << endl << "| 3.- Change         | 6.- Exit              |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "Your optios is: ";
        cin >> option;
        return option;
}

int main(){
        tail t;
        int o = 0, data = 0, search = 0;
        string valData = "";

        do{
                o = Menu(o);
                switch (o){
                        case 1:
                                cout << "Insert data for new node: ";
                                cin >> data;
                                t.insertNode(data);
				cout << endl << "Node insert in tail" << endl;
                        break;
                        case 2:
                                cout << "Insert data for search in tail: ";
                                cin >> search;
                                if (t.searchNode(search) == 1)
                                        valData = "True";
                                else
                                        valData = "False";
                                cout << endl << "The data: " << search << " found is: " << valData << endl;
                        break;
                        case 3:
                                cout << "Insert data for search in list: ";
                                cin >> search;
                                if (t.changeNode(search) == 1)
                                        valData = "Change data Node";
                                else
                                        valData = "Data is not found";
                                cout << endl << valData << endl;
                        break;
                        case 4:
                                cout << "Insert data for search in list: ";
                                cin >> search;
                                if (t.deleteNode(search) == 1)
                                        valData = "Data is delete";
                                else
                                        valData = "Data no Delete";
                                cout << endl << valData << endl;                               
                        break;
                        case 5:
                                cout << "The tail is:" << endl;
                                t.printTail();
                        break;
                        case 6:
                                cout << endl << "Exit the program" << endl << endl;
                        break;
                        default:
                                cout << endl << "No exists option" << endl;
                        break;
                }
        }while( o != 6);
}
