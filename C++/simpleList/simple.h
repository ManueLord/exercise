#ifndef __simple__
#define __simple__

typedef struct  node{
	int data;
	node* next;
}node;

class simple{
public:
	simple();
	~simple();

	node *first, *last;

	void insertNode(int);
	bool searchNode(int);
	bool changeNode(int);
	bool deleteNode(int);
	void printList();
};

#endif //__simple__
