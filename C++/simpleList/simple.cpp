#include "simple.h"
#include <iostream>
using namespace std;

simple::simple(){
	first = NULL;
	last = NULL;
}

simple::~simple(){
	delete first;
	delete last;
}

void simple::insertNode(int data){
	node *newNode = new node();
	newNode -> data = data;
	if (first == NULL){
		first = newNode;
		first -> next = NULL;
		last = newNode;
	} else {
		last -> next = newNode;
		newNode -> next = NULL;
		last = newNode;
	}
}

bool simple::searchNode(int search){
	node *aux = new node();
	bool found = false;
	aux = first;
	if (first != NULL){
		while (aux != NULL && found != true){
			if (aux -> data == search)
				found = true;
			aux = aux -> next;
		}
	} else
		cout << endl << "The empty list" << endl;
	return found;
}

bool simple::changeNode(int search){
	node *aux = new node();
	bool found = false;
	aux = first;
	if (first != NULL){
		while (aux != NULL && found != true){
			if (aux -> data == search){
				cout << "Insert new date to change: ";
				cin >> aux -> data;
				found = true;
			}
			aux = aux -> next;
		}
	} else 
		cout << endl << "The empty list" << endl;
	return found;
}

bool simple::deleteNode(int search){
	node *before = new node();
	node *aux = new node();
	bool found = false;
	before = NULL;
	aux = first;
	if (first != NULL){
		while (aux != NULL && found != true){
			if (aux -> data == search){
				if (aux == first) 
					first = first -> next;
				else if (aux == last){
					before -> next = NULL;
					last = before;
				} else
					before -> next = aux -> next;
				found = true;
			}
			before = aux;
			aux = aux -> next;
		}
	} else
		cout << endl << "The empty list" << endl;
	return found;
}

void simple::printList(){
	node *aux = new node();
	aux = first;
	if (first != NULL) {
		while (aux != NULL){
			cout << aux -> data << " ";
			aux = aux -> next;
		}
		cout << endl;
	} else 
		cout << endl << "The empyt List" << endl;
}
