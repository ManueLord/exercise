#include "simple.cpp"
#include <iostream>
#include <string>
using namespace std;

int Menu(int option){
        cout << endl << "|--------------------------------------------|";
        cout << endl << "|                Simple List                 |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "| 1.- Insert         | 4.- Delete            |";
        cout << endl << "| 2.- Search         | 5.- Print             |";
        cout << endl << "| 3.- Change         | 6.- Exit              |";
        cout << endl << "|--------------------------------------------|";
        cout << endl << "Your optios is: ";
        cin >> option;
        return option;
}

int main(){
	simple l;
	int o = 0, data = 0, search = 0;
    string valData = "";
    do{
        o = Menu(o);
        switch (o){
            case 1:
                cout << "Insert data to list: ";
                cin >> data;
                l.insertNode(data);
                cout << "Correct ADD deta in list" << endl;
                break;
            case 2:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (l.searchNode(search) == 1)
                    valData = "True";
                else
                    valData = "False";
                cout << endl << "The data: " << search << " found is: " << valData << endl;
                break;
            case 3:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (l.changeNode(search) == 1)
                    valData = "Change data Node";
                else
                    valData = "Data is not found";
                cout << endl << valData << endl;
                break;
            case 4:
                cout << "Insert data for search in list: ";
                cin >> search;
                if (l.deleteNode(search) == 1)
                    valData = "Data is delete";
                else
                    valData = "Data no Delete";
                cout << endl << valData << endl;                               
                break;
            case 5:
                cout << endl << "The List is:" << endl;
	            l.printList();
                break;
            case 6:
                cout << endl << "Exit the program" << endl << endl;
                break;
            default:
                cout << endl << "No exists option" << endl;
                break;
        }
    }while( o != 6);
    return 0;
}
