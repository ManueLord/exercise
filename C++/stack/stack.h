#ifndef __stack__
#define __stack__
#include<string>

struct node{
	int data;
	node *next;
} *first;

class stack{
public:
	void insertNode(int);
	bool searchNode(int);
	bool changeNode(int);
	bool deleteNode(int);
	void printStack();
};

#endif // __stack__
