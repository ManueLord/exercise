#include "stack.h"
#include <iostream>
#include <string>
using namespace std;

void stack::insertNode(int data){
	node* newNode = new node();
	newNode -> data = data;
	
	newNode -> next = first;
	first = newNode;

	cout << endl << "Node insert in stack" << endl;
}

bool stack::searchNode(int search){
	node* present = new node();
	bool found = false;
	present = first;
	while (present != NULL && found != true){
		if (present -> data == search){
			found = true;
		}
		present = present -> next;
	}
	return found;
}

bool stack::changeNode(int search){
	node* present = new node();
	bool found = false;
	present = first;
	while (present != NULL && found != true){
		if (present -> data == search){
			cout << endl << "The data to change is: " << search << endl;
			cout << "The New data is: ";
			cin >> present -> data;
			found = true;
		}
		present = present -> next;
	}	
	return found;
}

bool stack::deleteNode(int search){
	node* present = new node();
	node* before = new node();
	before = NULL;
	bool found = false;
	present = first;
	while (present != NULL && found != true){
		if (present -> data == search){
			if (present == first)
				first = first -> next;
			else
				before -> next = present -> next;
			found = true;
		}
		before = present;
		present = present -> next;
	}
	return found;
}

void stack::printStack(){
	node* present = new node();
	present = first;
	if(present != NULL){
		while (present != NULL){
			cout << present -> data << " ";
			present = present -> next;
		}
		cout << endl;
	} else {
		cout << endl << "the stack empty" << endl << endl;
	}
}
