#include "stack.cpp"
#include <iostream>
#include <string>
using namespace std;

int Menu(int option){
	cout << endl << "|--------------------------------------------|";
	cout << endl << "|                  Stack                     |";
	cout << endl << "|--------------------------------------------|";
	cout << endl << "| 1.- Insert         | 4.- Delete            |";
	cout << endl << "| 2.- Search         | 5.- Print             |";
	cout << endl << "| 3.- Change         | 6.- Exit              |";
	cout << endl << "|--------------------------------------------|";
	cout << endl << "Your optios is: ";
	cin >> option;
	return option;
}

int main(){
	stack s;
	int o = 0, data = 0, search = 0;
	string valData = "";

	do{
		o = Menu(o);
		switch (o){
			case 1:
				cout << "Insert data for new node: ";
				cin >> data;
				s.insertNode(data);
			break;
			case 2:
				cout << "Insert data for search in stack: ";
				cin >> search;
				if (s.searchNode(search) == 1)
					valData = "True";
				else
					valData = "False";
				cout << endl << "The data: " << search << " found is: " << valData << endl;
			break;
			case 3:
				cout << "Insert data for search in stack: ";
				cin >> search;
				if (s.changeNode(search) == 1)
					valData = "Change data Node";
				else
					valData = "Date is not found";
				cout << endl << valData << endl;
			break;
			case 4:
				cout << "Insert data for search in stack: ";
				cin >> search;
				if (s.deleteNode(search) == 1)
					valData = "Data is delete";
				else
					valData = "Data no Delete";
				cout << endl << valData << endl;
			break;
			case 5:
				cout << "The stack is:" << endl;
				s.printStack();
			break;
			case 6:
				cout << endl << "Exit the program" << endl << endl;
			break;
			default:
				cout << endl << "No exists option" << endl;
			break;
		}
	}while( o != 6);
	return 0;
}	
