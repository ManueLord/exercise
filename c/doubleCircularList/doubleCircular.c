#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int data;
	node* next;
	node* back;
}node;

node* first = NULL;
node* last = NULL;

void insertNode(int data) {
	node* newNode = (node*) malloc(sizeof(node));
	newNode -> data = data;
	if (first == NULL) {
		first = newNode;
		first -> next = first;
		last = first;
		first -> back = last;
	} else {
		last -> next = newNode;
		newNode -> next = first;
		newNode -> back = last;
		last = newNode;
		first -> back = last;
	}
}

bool searchNode(int search){
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search) {
				return true;
			}
			aux = aux -> next;
		} while (aux != first);
		printf("\n");
	} else{
		printf("\nThe empty list\n");
	}
	return false;
}

bool changeNode(int search){
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search) {
				printf("Insert new data to change: ");
				scanf("%d", &aux -> data);
				return true;
			}
			aux = aux -> next;
		} while (aux != first);
		printf("\n");
	} else{
		printf("\nThe empty list\n");
	}
	return false;
}

bool deleteNode(int search){
	node* aux = (node*) malloc(sizeof(node));
	node* before = (node*) malloc(sizeof(node));
	before = NULL;
	aux = first;
	if (first != NULL) {
		do{
			if (aux -> data == search) {
				if (aux == first) {
					first = first -> next;
					first -> back = last;
					last -> next = first;
				} else if (aux == last){
					before -> next = first;
					last = before;
					first -> back = last;
				}else{
					before -> next = aux -> next;
				}
				return true;
			}
			before = aux;
			aux = aux -> next;
		} while (aux != first);
		printf("\n");
	} else{
		printf("\nThe empty list\n");
	}
	return false;
}

void printListOrigin(){
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		do{
			printf("%d  ", aux -> data);
			aux = aux -> next;
		} while (aux != first);
		printf("\n");
	} else{
		printf("\nThe empty list\n");
	}
}

void printListReverse(){
	node* aux = (node*) malloc(sizeof(node));
	aux = last;
	if (first != NULL) {
		do {
			printf("%d  ", aux -> data);
			aux = aux -> back;
		} while (aux != last);
		printf("\n");
	} else {
		printf("\nThe empty list\n");
	}
}
