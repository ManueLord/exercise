#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int data;
	node* next;
	node* back;
}node;

node* first = NULL;
node* last = NULL;

void insertNode(int data) {
	node* newNode = (node*) malloc(sizeof(node));
	newNode -> data = data;
	if (first == NULL) {
		first = newNode;
		first -> next = NULL;
		first -> back = NULL;
		last = first;
	} else {
		last -> next = newNode;
		newNode -> next = NULL;
		newNode -> back = last;
		last = newNode;
	}
}

bool searchNode(int search) {
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		while (aux != NULL) {
			if (aux -> data == search) {
				return true;
			}			
			aux = aux -> next;
		}
		printf("\n");
	} else {
		printf ("The empty list");
	}
	return false;
}

bool changeNode(int search) {
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		while (aux != NULL) {
			if (aux -> data == search) {
				printf("Insert new data to change: ");
				scanf("%d", &aux -> data);
				return true;
			}			
			aux = aux -> next;
		}
		printf("\n");
	} else {
		printf ("The empty list");
	}
	return false;
}

bool deleteNode(int search) {
	node* aux = (node*) malloc(sizeof(node));
	node* before = (node*) malloc(sizeof(node));
	before = NULL;
	aux = first;	
	if (first != NULL) {
		while (aux != NULL) {
			if (aux -> data == search) {
				if (aux == first) {
					first = first -> next;
					first -> back = NULL;
				} else if (aux == last) {
					before -> next = NULL;
					last = before;
				} else {
					before -> next = aux -> next;
				}
				return true;
			}
			before = aux;
			aux = aux -> next;
		}
		printf("\n");
	} else {
		printf ("The empty list");
	}
	return false;
}

void printListOrigin() {
	node* aux = (node*) malloc(sizeof(node));
	aux = first;
	if (first != NULL) {
		while (aux != NULL) {
			printf("%d  ", aux -> data);
			aux = aux -> next;
		}
		printf("\n");
	} else {
		printf ("The empty list");
	}
}

void printListReverse() {
	node* aux = (node*) malloc(sizeof(node));
	aux = last;
	if (first != NULL) {
		while (aux != NULL) {
			printf("%d  ", aux -> data);
			aux = aux -> back;
		}
		printf("\n");
	} else {
		printf ("The empty list");
	}
}

