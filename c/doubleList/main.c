#include "doubleList.c"
#include <stdio.h>

int Menu (int option){
	printf("\n|--------------------------------------------|");
	printf("\n|               Double List                  |");
	printf("\n|--------------------------------------------|");
	printf("\n| 1.- Insert         | 5.- Print Origin      |");
	printf("\n| 2.- Search         | 6.- Print Reverse     |");
	printf("\n| 3.- Change         | 7.- Exit              |");
    printf("\n| 4.- Delete         |                       |");
	printf("\n|--------------------------------------------|");
	printf("\nYour optios is: ");
	scanf("%d", &option);
	return option;
}

int main(){
	int o = 0, data = 0, search = 0;

	do{
		o = Menu(o);
		switch (o){
			case 1:
				printf("Insert data for new node: ");
				scanf("%d", &data);
				insertNode(data);
			break;
			case 2:
				printf("Insert data for search in simple list: ");
				scanf("%d", &search);
				if (searchNode(search) == 1)
					printf("\nThe data: %d found is: True\n", search);
				else
					printf("\nThe data: %d found is: False\n", search);
			break;
			case 3:
				printf("Insert data for search in simple list: ");
				scanf("%d", &search);
				if (changeNode(search) == 1)
					printf("\nChange data Node\n");
				else
					printf("\nDate is not found\n");
			break;
			printf("\n\n");
			case 4:
				printf("Insert data for search in simple list: ");
				scanf("%d", &search);
				if (deleteNode(search) == 1)
					printf("\nData is delete\n");
				else
					printf("\nData no Delete\n");
			break;
			case 5:
				printf("The double list is:\n");
				printListOrigin();
			break;
            case 6:
				printf("The double list is:\n");
				printListReverse();
			break;
			case 7:
				printf("\nExit the program\n\n");
			break;
			default:
				printf("\nNo exists option\n");
			break;
		}
	} while (o != 7);
	return 0;
}